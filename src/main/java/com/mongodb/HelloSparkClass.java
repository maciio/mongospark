package com.mongodb;

import static spark.Spark.*;
/**
 * Created by Mario A. Pineda on 3/2/16.
 */
public class HelloSparkClass {
    public static void main(String[] args) {
        get("/hello", (request, response) -> "Hello Spark");
    }
}
